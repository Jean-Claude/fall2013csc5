/* 
 * File:   HelloWorld.cpp
 * Author: Jean-Claude Leotaud
 *
 * Created on August 28, 2013, 7:15 PM
 */

#import <cstdlib>
#import <iostream>

using namespace std;

int main()
{//Hello World Program
    
    string x="Hello World";
    cout << x << endl;
    
    return 0;
}
